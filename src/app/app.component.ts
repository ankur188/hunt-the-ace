import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Game';
  score: number = 0;
  round: number = 0;
  isGameInProgress:boolean = false;
  isFlipCard: boolean = false;
  cardsList:Array<any> = [{name: 'diamonds', url: './../assets/Ace_of_diamonds.svg', isFlip: false}, {name: 'clubs', url: './../assets/Ace_of_clubs.svg', isFlip: false},{name: 'spades', url: './../assets/Ace_of_spades.svg', isFlip: false},{name: 'hearts', url: './../assets/Ace_of_hearts.svg', isFlip: false}];
  shuffledCardList:Array<any> = [{name: 'diamonds', url: './../assets/Ace_of_diamonds.svg', isFlip: false}, {name: 'clubs', url: './../assets/Ace_of_clubs.svg', isFlip: false},{name: 'spades', url: './../assets/Ace_of_spades.svg', isFlip: false},{name: 'hearts', url: './../assets/Ace_of_hearts.svg', isFlip: false}];
  showMissed: boolean = false;
  showCongratulations: boolean = false;
  isCardSelected: boolean = true;
  cardPositions:Array<number> = [0,1,2,3];
  isShuffle: boolean = false;
  showOverlay:boolean = false;

  startGame() {
    this.shuffledCardList = [];
    this.isGameInProgress = true;
    this.showOverlay = true;
    this.score = 0;
    this.round++;
    this.isFlipCard = true;
    setTimeout(() => {
      this.showOverlay = false;
      setTimeout(() => {
        this.shuffle();
      }, 500);
    }, 1000);
  }
  
  openCard(cardName){
    if(!this.isGameInProgress)
    return
    this.isCardSelected = true;
    this.cardsList = this.cardsList.map(card => {
      if(card.name === cardName)
      card.isFlip = true;
      return card;
    })
    if(cardName === 'spades') {
      this.score++;
      this.showCongratulations = true;
    }
    else {
      this.showMissed = true;
    }
    setTimeout(() => {
      this.isFlipCard = false;
    }, 2000);

    setTimeout(() => {
      this.isFlipCard = true;
      this.cardsList= [{name: 'diamonds', url: './../assets/Ace_of_diamonds.svg', isFlip: false}, {name: 'clubs', url: './../assets/Ace_of_clubs.svg', isFlip: false},{name: 'spades', url: './../assets/Ace_of_spades.svg', isFlip: false},{name: 'hearts', url: './../assets/Ace_of_hearts.svg', isFlip: false}];
      this.shuffledCardList = [];
      this.cardPositions = [0,1,2,3]
      // this.isCardSelected = false;
      this.showCongratulations = false;
      this.showMissed = false;
      this.round++;
      this.showOverlay = true;
      setTimeout(() => {
        this.showOverlay = false;
        setTimeout(() => {
          this.shuffle();
        }, 1000);
      }, 1000);
    }, 4000);
  }

  shuffle() {
    this.isShuffle = true;
    setTimeout(() => {
      this.isShuffle = false;
      this.isCardSelected = false;
      for(let i=0; i<4; i++){
        var m = Math.floor(Math.random()*this.cardPositions.length);
        this.shuffledCardList.push(this.cardsList[this.cardPositions[m]])
        this.cardPositions.splice(m,1)
      }
      this.cardsList = JSON.parse(JSON.stringify(this.shuffledCardList));
    }, 5200);
    // console.log(111111, m);
    // this.cardPositions = this.cardPositions.splice(m,1);
  }

}

